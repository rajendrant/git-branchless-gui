## Publishing

```bash
# Install dependencies for both the extension and webview UI source code
npm run install:all

# Build webview UI source code
npm run build:webview

vsce publish
```

## Development Setup

### Extension

Open the directory in VS Code and run the extension from there. Following command will keep on building the typescript source code to the required js files.

`npm run esbuild-watch`

### webview-ui

Following command will keep on building the js and css to `public/build` for each of the `src/components/*`

`cd webview-ui && npm run dev`
