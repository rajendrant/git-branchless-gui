import * as childProcess from "child_process";
import * as vscode from "vscode";

export function runCommand(cmd: string, ws: string): Promise<[string | undefined, string, string]> {
  return new Promise((resolve) => {
    childProcess.exec(cmd, { cwd: ws }, (error, stdout, stderr) => {
      resolve([error?.message, stdout, stderr]);
    });
  });
}

export async function runCommandAndShowInTextDocument(cmd: string, ws: string) {
  const [error, stdout, stderr] = await runCommand(cmd, ws);
  let content = "";
  if (error) {
    content = `ERROR: ${error}\n\n`;
  }
  if (stderr) {
    content += `STDERR:\n${stderr}\n\n`;
  }
  content += `STDOUT:\n${stdout}`;
  vscode.workspace.openTextDocument({ content });
  return;
}

export async function runCommandInTerminal(cmd: string) {
  let t = vscode.window.activeTerminal;
  if (!t) {
    t = vscode.window.createTerminal();
  }
  t.sendText(cmd, true);
  t.show();
}
