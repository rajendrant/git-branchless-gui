import { formatDistanceToNow } from "date-fns";
import { CommitTree, getSmartLog, SmartLogInfo, gitMove } from "./git-utils";

export function convertToQuickPick(sl: SmartLogInfo, fromHash: string) {
  const res = [];
  for (const e of sl.tree) {
    if (fromHash === e.hash) {
      continue;
    }
    let first = e.hash,
      description = "";
    if (e.branchName) {
      first += `   (${e.branchName})`;
    }
    if (e.commitInfo) {
      description = `${formatDistanceToNow(new Date(e.commitInfo.commitTime * 1000))} by ${
        e.commitInfo.author
      }`;
    }

    res.push({
      label: first,
      description,
      detail: e.commitInfo?.commit,
      hash: e.hash,
    });
  }
  return res;
}
