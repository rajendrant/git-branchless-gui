import { assert } from "console";
import simpleGit, { ResetMode, SimpleGit } from "simple-git";

export enum CommitTreeBefore {
  vert = 0,
  slant = 1,
}

export enum CommitTreeAfter {
  none = 0,
  vert = 1,
  vertAndSlant = 2,
}

export class CommitInfo {
  longHash: string;
  commitTime: number;
  email: string;
  author: string;
  commit: string;
  constructor(longHash: string, commitTime: number, email: string, author: string, commit: string) {
    this.longHash = longHash;
    this.commitTime = commitTime;
    this.email = email;
    this.author = author;
    this.commit = commit;
  }
  serialize(): string[] {
    return [this.longHash, this.commitTime.toString(), this.email, this.author, this.commit];
  }
}

export class CommitTree {
  hash: string;
  numVerticals: number;
  before: CommitTreeBefore;
  after: CommitTreeAfter;
  lastVerticalHasSlant: number;
  commitInfo: CommitInfo | null = null;
  branchName: string = "";
  isFrozen = false;

  constructor(hash: string) {
    this.hash = hash;
    this.numVerticals = 0;
    this.before = CommitTreeBefore.vert;
    this.after = CommitTreeAfter.none;
    this.lastVerticalHasSlant = 0;
  }
  static deserialize(data: string[]): CommitTree {
    if (data.length !== 4) {
      return new CommitTree("");
    }
    const o = new CommitTree(data[0]);
    o.numVerticals = parseInt(data[1]);
    o.before = parseInt(data[2]);
    o.after = parseInt(data[3]);
    o.lastVerticalHasSlant = parseInt(data[4]);
    o.branchName = data[5];
    return o;
  }
  static serialize(o: CommitTree): [string[], string[]] {
    return [
      [
        o.hash,
        o.numVerticals.toString(),
        o.before.toString(),
        o.after.toString(),
        o.lastVerticalHasSlant.toString(),
        o.branchName,
        o.isFrozen ? "1" : "0",
      ],
      o.commitInfo ? o.commitInfo.serialize() : [],
    ];
  }
}

function parseSmartLog(smartlog: string): string[][] {
  const commits = [];
  const smartlogs = smartlog.split("\n");
  for (let i = 0; i < smartlogs.length; i++) {
    const split = smartlogs[i].split(" ");
    let j = 0;
    for (; j < split.length; j++) {
      if (!["o", "O", "@", "|", ":"].includes(split[j])) {
        break;
      }
    }
    if (j !== split.length && split[j].length >= 6) {
      let start = "";
      if (i !== 0) {
        start = smartlogs[i - 1];
      }
      commits.push([split[j], start, split.slice(0, j).join(" ")]);
    }
  }
  return commits;
}
async function abc() {
  const g = simpleGit(".");
  const slStr = await g.raw(["branchless", "smartlog"]);
  const sl = parseSmartLog(slStr);
  parseSmartLogTree(sl);
}
abc();

function isCommitChar(ch: string) {
  return ["O", "o", "@"].includes(ch);
}

function containsOnly(str: string, chars: string): boolean {
  for (const ch of str) {
    if (chars.indexOf(ch) === -1) {
      return false;
    }
  }
  return true;
}

export class SmartLogInfo {
  tree: CommitTree[];
  currentHash: string;
  mainHash: string;
  constructor(tree: CommitTree[], currentHash: string, mainHash: string) {
    this.tree = tree;
    this.currentHash = currentHash;
    this.mainHash = mainHash;
  }
}

function parseSmartLogTree(commits: string[][]): SmartLogInfo {
  if (commits[0][1] === ":") {
    commits[0][1] = "";
  }
  const tree: CommitTree[] = [];
  let mainHash = "",
    currentHash = "";
  for (let i = 0; i < commits.length; i++) {
    const [commit, t0, t1] = commits[i];
    const entry = new CommitTree(commit);
    assert(containsOnly(t0, ":| \\"));
    assert(containsOnly(t1.slice(0, -1), "| \\"));
    const t1Last = t1.slice(-1);
    assert(isCommitChar(t1Last));
    if (t1Last === "O") {
      mainHash = commit;
      entry.isFrozen = true;
    } else if (t1Last === "@") {
      currentHash = commit;
    }
    for (let j = 0; j < t1.length; j++) {
      if (t1[j] === " ") {
        continue;
      }
      if (t0 === ":") {
        // continue;
      }
      if ("|:".includes(t1[j]) && "|:".includes(t0[j])) {
        entry.numVerticals += 1;
      }
    }
    if (isCommitChar(t1.slice(-1)) && t0.slice(-1) === "\\") {
      entry.before = CommitTreeBefore.slant;
      if (tree[tree.length - 1].after === CommitTreeAfter.none) {
        tree[tree.length - 1].lastVerticalHasSlant = 1;
      }
    }
    if (i + 1 < commits.length) {
      const nextCommit = commits[i + 1];
      if (nextCommit[1].length === t1.length && nextCommit[1].slice(-1) === "|") {
        entry.after = CommitTreeAfter.vert;
      } else if (nextCommit[1].length > t1.length && nextCommit[1].slice(-1) === "\\") {
        entry.after = CommitTreeAfter.vertAndSlant;
      }
    }
    tree.push(entry);
  }
  return new SmartLogInfo(tree, currentHash, mainHash);
}

export async function gitCommitInfoPopulate(g: SimpleGit, tree: CommitTree[]): Promise<boolean> {
  const commitInfos = (
    await g.show(
      ["--no-patch", "--no-notes", "--pretty=%H|&$|%at|&$|%ae|&$|%an|&$|%s"].concat(tree.map((x) => x.hash))
    )
  ).split("\n", tree.length);
  if (commitInfos.length !== tree.length) {
    return false;
  }
  for (let i = 0; i < commitInfos.length; i++) {
    const res = commitInfos[i].split("|&$|");
    tree[i].commitInfo = new CommitInfo(res[0], parseInt(res[1]), res[2], res[3], res[4]);
  }
  return true;
}

export async function getSmartLog(dir: string) {
  const g = simpleGit(dir);
  const slStr = await g.raw(["branchless", "smartlog"]);
  const sl = parseSmartLog(slStr);
  const tree = parseSmartLogTree(sl);
  await gitCommitInfoPopulate(g, tree.tree);
  const branches = await g.branch(["--no-abbrev"]);
  for (const [_, b] of Object.entries(branches.branches)) {
    if (b.current) {
      // tree.currentHash = b.commit;
    }
    for (const t of tree.tree) {
      if (t.commitInfo?.longHash === b.commit) {
        t.branchName = b.name;
        break;
      }
    }
  }
  return tree;
}

export async function getGitStatusFast(dir: string) {
  const g = simpleGit(dir);
  return g.status({ "--untracked-files": "no" });
}

export async function getGitStatus(dir: string) {
  const g = simpleGit(dir);
  return g.status();
}

export async function gitStageFile(dir: string, fname: string) {
  const g = simpleGit(dir);
  return g.add(fname);
}

export async function gitUnstageFile(dir: string, fname: string) {
  const g = simpleGit(dir);
  // return g.reset(ResetMode.SOFT, [fname]);
  return g.raw(["restore", "--staged", fname]);
}

export async function gitCheckout(dir: string, hash: string) {
  const g = simpleGit(dir);
  return g.checkout(hash);
}

export async function gitMove(dir: string, from: string, to: string) {
  const g = simpleGit(dir);
  return g.raw(["move", "-s", from, "-d", to, "--merge"]);
}

// getSmartLog(process.cwd());

//git show -s --format=%B SHA1
// git show --no-patch --no-notes --pretty='%cd %ct' SHA1
// git show --no-patch --no-notes --pretty='%H %h %at %ae %an %n%B'  bcc2e64
