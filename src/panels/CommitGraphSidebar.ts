import * as vscode from "vscode";
import { getNonce } from "../utils/getNonce";
import { getUri } from "../utils/getUri";
import { CommitTree, getSmartLog, SmartLogInfo, gitMove } from "../utils/git-utils";
import simpleGit from "simple-git";
import * as commitGraphUtils from "../utils/CommitGraphUtils";
import * as utils from "../utils/utils";

export class CommitGraphSidebar implements vscode.WebviewViewProvider {
  _view?: vscode.WebviewView;
  _doc?: vscode.TextDocument;

  constructor(private readonly _extensionUri: vscode.Uri) {}

  public resolveWebviewView(webviewView: vscode.WebviewView) {
    this._view = webviewView;

    webviewView.webview.options = {
      enableScripts: true,
      localResourceRoots: [this._extensionUri],
    };

    webviewView.webview.html = this._getHtmlForWebview(webviewView.webview);

    webviewView.onDidDispose(() => {
      this._view = undefined;
    });

    webviewView.webview.onDidReceiveMessage(async (data) => {
      if (!vscode.workspace.workspaceFolders) {
        return;
      }
      const ws = vscode.workspace.workspaceFolders[0].uri.fsPath;
      console.log(ws);
      switch (data.type) {
        case "onInfo": {
          if (!data.value) {
            return;
          }
          vscode.window.showInformationMessage(data.value);
          break;
        }
        case "onError": {
          if (!data.value) {
            return;
          }
          vscode.window.showErrorMessage(data.value);
          break;
        }
        case "getTree": {
          this._updateSmartLog(ws);
          break;
        }
        case "moveToHash": {
          webviewView.webview.postMessage({ type: "refreshStart" });
          const hash = data.value;
          const g = simpleGit(ws);
          try {
            const status = await g.status({ "--untracked-files": "no" });
            if (!status.isClean()) {
              if (
                (await vscode.window.showErrorMessage(
                  "Tree is not clean. Do you still want to continue to switch to the commit.",
                  "Continue",
                  "Cancel"
                )) !== "Continue"
              ) {
                this._updateSmartLog(ws);
                return;
              }
            }
            await g.checkout(hash);
          } catch (e) {
            vscode.window.showErrorMessage("Git operation failed - " + e);
          }
          this._updateSmartLog(ws);
          break;
        }
        case "rebaseTo": {
          const sl = await getSmartLog(ws);
          const fromHash = data.value;
          if (!fromHash) {
            return;
          }
          const toHash = await vscode.window.showQuickPick(
            commitGraphUtils.convertToQuickPick(sl, fromHash),
            {
              title: `Choose the destination commit to move the source commit '${fromHash}' to`,
            }
          );
          if (!toHash) {
            return;
          }
          webviewView.webview.postMessage({ type: "refreshStart" });
          try {
            const res = await gitMove(ws, fromHash, toHash.hash);
            console.error(res);
          } catch (e) {
            vscode.window.showErrorMessage("Git move failed - " + e);
          }
          this._updateSmartLog(ws);
          break;
        }
        case "customMenuActions": {
          this._updateCustomMenuActions();
          break;
        }
        case "runAction": {
          console.error("runAction", data.value);
          const g = simpleGit(ws);
          webviewView.webview.postMessage({ type: "refreshStart" });
          try {
            if (data.value !== "commit" && data.value !== "amend") {
              return;
            }
            const status = await g.status({ "--untracked-files": "no" });
            const files = [];
            files.push(...status.modified);
            files.push(...status.staged);
            if (!files.length) {
              vscode.window.showErrorMessage("No changes to commit");
              return;
            }
            let res: any;
            if (data.value === "commit") {
              res = await g.commit("msg", files);
            } else if (data.value === "amend") {
              res = await g.raw(["amend"]);
            }
            console.error(res);
            vscode.commands.executeCommand("git.refresh");
            vscode.window.showInformationMessage("Changes committed/amended successfully.");
          } catch (e) {
            vscode.window.showErrorMessage("Git commit/amend failed - " + e);
          }
          this._updateSmartLog(ws);
          break;
        }
        case "runCustomMenuAction": {
          webviewView.webview.postMessage({ type: "refreshStart" });
          await this._runCustomMenuAction(ws, data.value);
          this._updateSmartLog(ws);
          vscode.commands.executeCommand("git.refresh");
          break;
        }
        case "hideCommit": {
          webviewView.webview.postMessage({ type: "refreshStart" });
          const g = simpleGit(ws);
          try {
            await g.raw(["hide", data.value]);
            vscode.window.showInformationMessage("Commit hidden successfully.");
          } catch (e) {
            vscode.window.showErrorMessage("Git hide commit failed - " + e);
          }
          this._updateSmartLog(ws);
          break;
        }
      }
    });

    vscode.workspace.onDidChangeConfiguration((ev) => {
      if (ev.affectsConfiguration("gitBranchless.customMenuActions")) {
        this._updateCustomMenuActions();
      }
    });
  }

  public revive(panel: vscode.WebviewView) {
    this._view = panel;
  }

  public async updateSmartLog() {
    if (!vscode.workspace.workspaceFolders) {
      return;
    }
    const ws = vscode.workspace.workspaceFolders[0].uri.fsPath;
    this._updateSmartLog(ws);
  }

  private async _updateSmartLog(ws: string) {
    const sl = await getSmartLog(ws);
    this._view?.webview.postMessage({
      type: "getTree",
      value: {
        tree: sl.tree.map(CommitTree.serialize),
        currentHash: sl.currentHash,
        mainHash: sl.mainHash,
      },
    });
  }

  private async _updateCustomMenuActions() {
    const menu = vscode.workspace
      .getConfiguration("gitBranchless")
      .get<{ name: string; command: string }[]>("customMenuActions");
    if (!menu) {
      return;
    }
    const customMenuActions: string[] = [];
    for (const m of menu) {
      customMenuActions.push(m.name);
    }
    this._view?.webview.postMessage({
      type: "customMenuActions",
      value: customMenuActions,
    });
  }

  private async _runCustomMenuAction(ws: string, name: string) {
    if (!ws || !name) {
      return;
    }
    const menu = vscode.workspace
      .getConfiguration("gitBranchless")
      .get<{ name: string; command: string }[]>("customMenuActions");
    if (!menu) {
      return;
    }
    for (const m of menu) {
      if (m.name !== name) {
        continue;
      }
      utils.runCommandInTerminal(m.command);
      return;
    }
  }

  private _getHtmlForWebview(webview: vscode.Webview) {
    const scriptUri = getUri(webview, this._extensionUri, [
      "webview-ui",
      "public",
      "build",
      "CommitGraphSideBar.js",
    ]);
    const cssUri = getUri(webview, this._extensionUri, [
      "webview-ui",
      "public",
      "build",
      "CommitGraphSideBar.css",
    ]);
    const nonce = getNonce();
    const cssCommonUri = webview.asWebviewUri(
      vscode.Uri.joinPath(this._extensionUri, "assets", "common.css")
    );
    return `<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<!--
					Use a content security policy to only allow loading images from https or from our extension directory,
					and only allow scripts that have a specific nonce.
        -->
        <meta http-equiv="Content-Security-Policy" content="img-src https: data:; style-src 'unsafe-inline' ${webview.cspSource}; script-src 'nonce-${nonce}'">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="${cssUri}" rel="stylesheet">
        <link href="${cssCommonUri}" rel="stylesheet">
        <script defer nonce="${nonce}" src="${scriptUri}"></script>
        </head>
      <body style="padding: 0;">
			</body>
			</html>`;
  }
}
