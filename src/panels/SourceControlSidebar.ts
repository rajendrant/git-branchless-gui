import * as vscode from "vscode";
import { getNonce } from "../utils/getNonce";
import { getUri } from "../utils/getUri";
import * as gitutils from "../utils/git-utils";
import { basename, dirname } from "path";

export class SourceControlSidebar implements vscode.WebviewViewProvider {
  _view?: vscode.WebviewView;
  _doc?: vscode.TextDocument;

  constructor(private readonly _extensionUri: vscode.Uri) {}

  public resolveWebviewView(webviewView: vscode.WebviewView) {
    this._view = webviewView;

    webviewView.webview.options = {
      enableScripts: true,
      localResourceRoots: [this._extensionUri],
    };

    webviewView.webview.html = this._getHtmlForWebview(webviewView.webview);

    webviewView.webview.onDidReceiveMessage(async (data) => {
      if (!vscode.workspace.workspaceFolders) {
        return;
      }
      const ws = vscode.workspace.workspaceFolders[0].uri.fsPath;
      switch (data.type) {
        case "onInfo": {
          if (!data.value) {
            return;
          }
          vscode.window.showInformationMessage(data.value);
          break;
        }
        case "onError": {
          if (!data.value) {
            return;
          }
          vscode.window.showErrorMessage(data.value);
          break;
        }
        case "gitStatus": {
          this._gitStatusUpdate(ws);
          break;
        }
        case "stageFile": {
          await gitutils.gitStageFile(ws, data.value);
          this._gitStatusUpdate(ws);
          break;
        }
        case "unstageFile": {
          await gitutils.gitUnstageFile(ws, data.value);
          this._gitStatusUpdate(ws);
          break;
        }
      }
    });
  }

  public revive(panel: vscode.WebviewView) {
    this._view = panel;
  }

  private async _gitStatusUpdate(tree: string) {
    const status = await gitutils.getGitStatusFast(tree);
    this._view?.webview.postMessage({
      type: "gitStatus",
      value: {
        status: status.files.map((x) => [
          x.path,
          basename(x.path),
          dirname(x.path),
          x.index,
          x.working_dir,
          x.from,
        ]),
        isClean: status.isClean(),
      },
    });
  }

  private _getHtmlForWebview(webview: vscode.Webview) {
    const scriptUri = getUri(webview, this._extensionUri, [
      "webview-ui",
      "public",
      "build",
      "SourceControlSideBar.js",
    ]);
    const cssUri = getUri(webview, this._extensionUri, [
      "webview-ui",
      "public",
      "build",
      "SourceControlSideBar.css",
    ]);
    const nonce = getNonce();
    const cssCommonUri = webview.asWebviewUri(
      vscode.Uri.joinPath(this._extensionUri, "assets", "common.css")
    );

    return `<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<!--
					Use a content security policy to only allow loading images from https or from our extension directory,
					and only allow scripts that have a specific nonce.
        -->
        <meta http-equiv="Content-Security-Policy" content="img-src https: data:; style-src 'unsafe-inline' ${webview.cspSource}; script-src 'nonce-${nonce}';">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="${cssUri}" rel="stylesheet">
        <link href="${cssCommonUri}" rel="stylesheet">
        <script defer nonce="${nonce}" src="${scriptUri}"></script>
        </head>
      <body style="padding: 0;">
			</body>
			</html>`;
  }
}
