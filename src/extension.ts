import * as vscode from "vscode";
import { CommitGraphSidebar } from "./panels/CommitGraphSidebar";
import { SourceControlSidebar } from "./panels/SourceControlSidebar";

export function activate(context: vscode.ExtensionContext) {
  const sideBar = new CommitGraphSidebar(context.extensionUri);
  context.subscriptions.push(
    vscode.window.registerWebviewViewProvider("gitBranchlessCommitGraph", sideBar),
    vscode.commands.registerCommand("gitBranchless.refreshCommitGraph", () => {
      sideBar.updateSmartLog();
    })
    // vscode.window.registerWebviewViewProvider(
    //   "source-control",
    //   new SourceControlSidebar(context.extensionUri)
    // )
  );
}
