import App from "../components/SourceControlSideBar.svelte";

const app = new App({
  target: document.body,
  props: {},
});

export default app;
